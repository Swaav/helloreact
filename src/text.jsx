import React from 'react';

function Text(props) {
    return <span style={{color: props.color}}>{props.value}</span>
  }

  export default Text